Document all concepts and your implementation decisions.

---


#Return values from yahoo
Values available in the test urls.

    s   Symbol
    l1  Last Trade (Price Only)
    d1  Date of Last Trade
    t1  Time of Last Trade
    c1  Change (in points)
    o   Open price
    h   Day’s High
    g   Day’s Low
    v   Volume

All return values

    a	Ask
    a2	Average Daily Volume
    a5	Ask Size
    b	Bid
    b2	Ask (Real-time)
    b3	Bid (Real-time)
    b4	Book Value
    b6	Bid Size
    c	Change and Percent Change
    c1	Change
    c3	Commission
    c6	Change (Real-time)
    c8	After Hours Change (Real-time)
    d	Dividend/Share
    d1	Last Trade Date
    d2	Trade Date
    e	Earnings/Share
    e1	Error Indication (returned for symbol changed / invalid)
    e7	EPS Est. Current Yr
    e8	EPS Estimate Next Year
    e9	EPS Est. Next Quarter
    f6	Float Shares
    g	Day’s Low
    g1	Holdings Gain Percent
    g3	Annualized Gain
    g4	Holdings Gain
    g5	Holdings Gain Percent (Real-time)
    g6	Holdings Gain (Real-time)
    h	Day’s High
    i	More Info
    i5	Order Book (Real-time)
    j	52-week Low
    j1	Market Capitalization
    j3	Market Cap (Real-time)
    j4	EBITDA
    j5	Change From 52-week Low
    j6	Percent Change From 52-week Low
    k	52-week High
    k1	Last Trade (Real-time) With Time
    k2	Change Percent (Real-time)
    k3	Last Trade Size
    k4	Change From 52-wk High
    k5	Percent Change From 52-week High
    l	Last Trade (With Time)
    l1	Last Trade (Price Only)
    l2	High Limit
    l2	High Limit
    l3	Low Limit
    l3	Low Limit
    m	Day's Range
    m2	Day’s Range (Real-time)
    m3	50-day Moving Avg
    m4	200-day Moving Average
    m5	Change From 200-day Moving Avg
    m6	Percent Change From 200-day Moving Average
    m7	Change From 50-day Moving Avg
    m8	Percent Change From 50-day Moving Average
    n	Name
    n4	Notes
    o	Open
    p	Previous Close
    p1	Price Paid
    p2	Change in Percent
    p5	Price/Sales
    p6	Price/Book
    q	Ex-Dividend Date
    q	Ex-Dividend Date
    r	P/E Ratio
    r1	Dividend Pay Date
    r2	P/E (Real-time)
    r5	PEG Ratio
    r6	Price/EPS Est. Current Yr
    r7	Price/EPS Estimate Next Year
    s	Symbol
    s1	Shares Owned
    s7	Short Ratio
    s7	Short Ratio
    t1	Last Trade Time
    t6	Trade Links
    t7	Ticker Trend
    t8	1 yr Target Price
    v	Volume
    v1	Holdings Value
    v7	Holdings Value (Real-time)
    v7	Holdings Value (Real-time)
    w	52-week Range
    w	52-week Range
    w1	Day's Value Change
    w1	Day’s Value Change
    w4	Day's Value Change (Real-time)
    w4	Day’s Value Change (Real-time)
    x	Stock Exchange
    x	Stock Exchange
    y	Dividend Yield
    y	Dividend Yield

### Get the company name with a ticker symbol
    http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=TICKER_SYMBOL&callback=YAHOO.Finance.SymbolSuggest.ssCallback

Replace `TICKER_SYMBOL` with your ticket symbol.


#Flow of the program
TODO: Improve this flow

1. init
1. retrieve data
1. draw data
1. go to step 2


#Concepts
For every concept the following items:

- short description
- code example
- reference to mandatory documentation
- reference to alternative documentation (authoritative and authentic)

###Objects, including object creation and inheritance
TODO:

###websockets
TODO:

Code example

    (function () {
        window.stockquote = {
            stocks: [],
            init: function () {
            }
        }
    }());

###XMLHttpRequest
TODO:

###AJAX
TODO:

###Callbacks
TODO:

###How to write testable code for unit-tests
TODO:

